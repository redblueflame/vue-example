import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    checkboxStatus: null,
    loading: true,
  },
  mutations: {
    changeCheckboxStatus: (state, payload) => {
      console.log("Testing mutation");
      state.checkboxStatus = payload;
    },
    changeLoadingStatus: (state, payload) => {
      console.log("Changed loading status !");
      state.loading = payload;
    }
  },
  actions: {
    getStatusFromApi: context => {
      // Get a value from a website.
      // Pour l'exemple, j'ai mis une valeur aléatoire
      context.commit('changeLoadingStatus', true);
      let result = Math.random() < 0.5;
      // On attends pour voir ce que ca rend
      setTimeout(() => {
        // On change le statut.
        context.commit('changeCheckboxStatus', result);
        context.commit('changeLoadingStatus', false);
      }, 5000)
    }
  },
  getters: {
    getCheckBoxStatus: (state) => {
      if (state.checkboxStatus === null) {
        return false;
      }
      return state.checkboxStatus
    },
    getLoadingStatus: (state) => {
      if (state.loading === null) {
        return false;
      }
      return state.loading;
    }
  },
  modules: {
  }
})
